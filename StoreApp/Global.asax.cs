﻿using AutoMapper;
using StoreApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace StoreApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Category, CategoryViewModel>();

                config.CreateMap<AddProductViewModel, Product>().ForMember(s => s.Tags, opts => opts.Ignore()).
                AfterMap((s,d) => {
                    d.Id = Guid.NewGuid();
                    d.Tags =
                    s.Tags.Select(t => new Tag { Id = Guid.NewGuid(), Name = t }).ToList();
                    d.CategoryId = s.Categories.Last().Id;

                });

                config.CreateMap<Picture,PictureViewModel>();

                config.CreateMap<Product, DisplayProductViewModel>().AfterMap((s,d) =>
                {
                    var category = s.Category;

                    while (category != null)
                    {
                        d.Categories.Add(new DisplayCategoryViewModel { Name = category.Name });

                        category = category.UpperCategory;
                    }

                    d.ProfilePicture = Mapper.Map<DisplayPictureViewModel>(s.Pictures.Single(p => p.Order == 1));
                }
                );

                config.CreateMap<Picture, DisplayPictureViewModel>(); 

                config.CreateMap<Tag, DisplayTagViewModel>();


            });

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
