﻿using Ninject;
using StoreApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace StoreApp.Utils
{
    public class MinimumNumberOfElementsAttribute : ValidationAttribute, IClientValidatable
    {
        public int MinimumCount { get; set; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(ErrorMessageString, MinimumCount),
                
                ValidationType = "minimumcount"
            };

            rule.ValidationParameters.Add("count", MinimumCount);

            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            var tags = ((IEnumerable<object>)value);

            if (tags == null || tags.Count() < MinimumCount)
                return new ValidationResult(string.Format(ErrorMessageString, MinimumCount));

                return ValidationResult.Success;
        }
    }

    public class HasValidCategoriesAttribute : ValidationAttribute, IClientValidatable
    {
        [Inject]
        public IStoreRepository StoreRepository { get; set; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = ErrorMessage,

                ValidationType = "hasvalidcategories"
            };

            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {            
            var modelType = validationContext.ObjectInstance.GetType();

            var categories = modelType.GetProperty("Categories");

            var ids = ((List<CategoryIdViewModel>)categories.GetValue(validationContext.ObjectInstance)).Select(c => c.Id).ToArray();

            var validCategories = StoreRepository.HasValidCategories(ids);

            var validCategoriesProp = modelType.GetProperty("ValidCategories");

            validCategoriesProp.SetValue(validationContext.ObjectInstance, validCategories);

            if (validCategories)
            {


                return ValidationResult.Success;
            }

            

            return new ValidationResult(ErrorMessage);
        }
    }
}