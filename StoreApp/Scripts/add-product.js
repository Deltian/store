﻿
// tagi

$('#addtag').bind("click", function () {
    var tag = $("#addtags").val();

    if (tag) {
        var $template = $($("#template").html());

        var $input = $template.find("input");

        $input.before(tag);

        $input.val(tag);

        $("#taglist").append($template);
    }
});

$("#taglist").on("click", ".glyphicon-remove", function () {

    var $li = $(this).parent();

    $li.remove();
});

// kategorie

$(document).on("change", ".categories select", function () {
    var $this = $(this);

    var index = parseInt($this.data("index"));

    $.get('/Sales/GetCategories', { id: $this.val(), index: ++index }, function (data, textStatus, jqxhr) {


        if (!data.allcategories) {
            var allcategories = jqxhr.getResponseHeader("allcategories")

            $("#allcategories").val(allcategories);
        }
        else {
            $("#allcategories").val(data.allcategories);
        }

        $this.parent().nextUntil("br").remove();

        if (!data.allcategories)
            $(data).insertAfter($this.parent());

        $('#addproduct').data('validator', null);
        $.validator.unobtrusive.parse($('#addproduct'));
    });
});

// obrazki

$("#image").bind("change", function (e) {

    var file = e.target.files[0];

    var formData = new FormData();

    formData.append("image", file);

    formData.append("index", $("#images li").length);

    $.ajax("/Sales/GetImage",
        {
            type: "post",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data) {
                $("#images").append(data);
            },
            error: function (xhr, status, p3, p4) {
                $("#response").html(xhr.responseText)
            }
    });
});

$("#images").on("click", ".remove", function () {

    $(this).parent().remove();

    var $images = $("#images li");

    for (var i = 0; i < $images.length; i++) {
        $($images[i]).children("input:first").attr("name", "Pictures[" + i + "].Extension")
        $($images[i]).children("input:last").attr("name", "Pictures[" + i + "].Id")
    }

    
});

// validator dla kategorii

$.validator.unobtrusive.adapters.add('hasvalidcategories', [], function (options) {
    options.rules['hasvalidcategories'] = true;
    options.messages['hasvalidcategories'] = options.message;
});

$.validator.addMethod("hasvalidcategories", function (value, element) {

    var valid = $("#allcategories").val();

    return valid == 'true';
});

// validator count

$.validator.unobtrusive.adapters.add('minimumcount', ["count"], function (options) {
    options.rules['minimumcount'] = options.params.count;
    options.messages['minimumcount'] = options.message;
});

$.validator.addMethod("minimumcount", function (value, element, param) {

    var count = $(element).prev().children().length;

    return parseInt(param) <= count;
});