﻿using AutoMapper;
using StoreApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace StoreApp
{
    public interface IStoreRepository
    {
        IEnumerable<SelectListItem> GetLowerCategories(int id, int selectedVal);
        IEnumerable<CategoryViewModel> GetCategories();
        IEnumerable<SelectListItem> GetCategoryListItems(int selectedVal, bool defaultVal = false);
        bool HasValidCategories(int[] categoryIds);
        void AddProduct(AddProductViewModel model);
        IEnumerable<DisplayProductViewModel> SearchProductsBy(SearchViewModel model);
    }

    public class StoreRepository : IStoreRepository
    {
        private ApplicationDbContext _context;

        public StoreRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetLowerCategories(int id, int selectedVal)
        {
            var categories = _context.Categories.Include(c => c.LowerCategories.Select(c2 => c2.LowerCategories)).Take(6);

            return categories.SingleOrDefault(c => c.Id == id).LowerCategories.Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString(), Selected = c.Id == selectedVal });
        }

        public IEnumerable<CategoryViewModel> GetCategories()
        {
            var categories = _context.Categories.Include(c => c.LowerCategories.Select(c2 => c2.LowerCategories)).Take(6);

            return Mapper.Map<IEnumerable<CategoryViewModel>>(categories);
        }

        public IEnumerable<SelectListItem> GetCategoryListItems(int selectedVal, bool defaultVal = false)
        {
            var selectList = _context.Categories.
                Take(6).Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString(), Selected = c.Id == selectedVal }).ToList();

            if (defaultVal)
                selectList.Insert(0, new SelectListItem { Text = "Wszystkie kategorie", Value = "0" });

            return selectList;
        }

        public bool HasValidCategories(int[] categoryIds)
        {
            var categories = _context.Categories.Include(c => c.LowerCategories.Select(c2 => c2.LowerCategories)).Take(6);


            return ValidateCategories(categories, categoryIds);
        }

        private bool ValidateCategories(IEnumerable<Category> categories, int[] categoryIds, int index = 0)
        {
            if (index == categoryIds.Length)
                return true;


            var category = categories.SingleOrDefault(c => c.Id == categoryIds[index]);

            if (category == null)
                return false;

            return ValidateCategories(category.LowerCategories, categoryIds, ++index);
        }

        public void AddProduct(AddProductViewModel model)
        {
            var product = Mapper.Map<Product>(model);

            for (int i = 0; i < product.Pictures.Count; i++)
            {
                product.Pictures.ElementAt(i).Order = i + 1;
            }

            _context.Products.Add(product);
            _context.SaveChanges();
        }

        public IEnumerable<DisplayProductViewModel> SearchProductsBy(SearchViewModel model)
        {
            var products = _context.Products.Include(p => p.Tags).Include(p => p.Category.UpperCategory.UpperCategory).Include(p => p.Pictures).AsEnumerable();

            bool filterProducts(Product product)
            {
                return product.Tags.Any(t => t.Name.ToLowerInvariant().Contains(model.Query.ToLowerInvariant())
                    || product.Name.ToLowerInvariant().Contains(model.Query.ToLowerInvariant()));
            }


            bool filterByTopCategory(Product product)
            {
                Category category = product.Category;

                while (category.UpperCategory != null)
                {
                    category = category.UpperCategory;
                }

                return category.Id == model.Category;
            }

            if (model.Query != null)
                products = products.Where(filterProducts);

            if (model.Category != 0)
                products = products.Where(filterByTopCategory);

            return Mapper.Map<IEnumerable<DisplayProductViewModel>>(products);
        }
    }
}