﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StoreApp.Controllers
{
    public class HomeController : Controller
    {
        private IStoreRepository _storeRepository;
        private ApplicationUserManager _userManager;

        public HomeController(ApplicationUserManager userManager, IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository;
            _userManager = userManager;
            ViewBag.CategoryOptions = _storeRepository.GetCategoryListItems(0, true);

        }

        public ActionResult Index(bool? autoOpen)
        {
            ViewBag.Categories = _storeRepository.GetCategories();

            if (autoOpen.HasValue)
                TempData["autoOpen"] = autoOpen.Value;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}