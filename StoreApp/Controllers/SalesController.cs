﻿using AutoMapper;
using StoreApp.Models;
using StoreApp.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StoreApp.Controllers
{
    public class SalesController : Controller
    {
        private ApplicationUserManager _userManager;
        private IStoreRepository _storeRepository;

        public SalesController(ApplicationUserManager userManager, IStoreRepository storeRepositry)
        {
            _userManager = userManager;
            _storeRepository = storeRepositry;
            ViewBag.CategoryOptions = _storeRepository.GetCategoryListItems(0, true);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddProduct()
        {


            ViewBag.Categories = new[] { _storeRepository.GetCategoryListItems(0) };

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProduct(AddProductViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var categories = new List<IEnumerable<SelectListItem>> { _storeRepository.GetCategoryListItems(model.Categories[0].Id) };

                for (var i = 0; i < model.Categories.Count-1; i++)
                {
                    categories.Add(_storeRepository.GetLowerCategories(model.Categories[i].Id, model.Categories[i+1].Id));
                }

                ViewBag.Categories = categories;

                return View(model);
            }

            _storeRepository.AddProduct(model);

            TempData["success"] = "Dodano pomyślnie produkt";

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Search(SearchViewModel model)
        {
            var products = _storeRepository.SearchProductsBy(model);

            ViewBag.Query = model.Query;

            ViewBag.CategoryId = model.Category;

            var categories = _storeRepository.GetCategories();

            ViewBag.Categories = categories;

            ViewBag.Category = model.Category == 0 ? "wszystkie kategorie" : categories.First(c => c.Id == model.Category).Name;

            return View(products);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult GetImage(int index)
        {
            ViewBag.Index = index;

            var image = Request.Files[0];

            var path = Server.MapPath("~/Uploads/");

            var guid = Guid.NewGuid();

            var extension = image.FileName.Split('.').Last();

            var targetFolder = Path.Combine(path, guid + "." + extension);

            image.SaveAs(targetFolder);

            var model = new PictureViewModel
            {
                Id = guid,
                Extension = extension
            };

            return PartialView(model);
        }

        [AjaxOnly]
        public ActionResult GetCategories(int? id, int index)
        {
            if (!id.HasValue)
            {
                // w razie wyboru opcji wybierz, dane z kategoriami są niepoprawne

                return Json(new { allcategories = "false" }, JsonRequestBehavior.AllowGet);
            }
            var categories = _storeRepository.GetLowerCategories(id.Value, 0);

            ViewBag.Index = index;



            if (categories.Any())
            {
                // dodaj wyższy szczebel kategorii

                this.Response.Headers.Add("allcategories", "false");

                ViewBag.Categories = new[] { categories };

                ViewBag.Index2 = 0;

                return PartialView("_CategoryPartial", new AddProductViewModel());
            }

            // po wyborze danej kategorii pod parametrem id, jeśli do tej kategorii nie ma przypisanych niższych kategorii, dane z kategoriami są poprawne

            return Json(new { allcategories = "true" }, JsonRequestBehavior.AllowGet); ;
        }
    }
}