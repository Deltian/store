﻿using StoreApp.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StoreApp.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Display(Name = "UserName",ResourceType = typeof(LoginRegisterDisplayNames))]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(LoginRegisterDisplayNames))]
        public string Password { get; set; }

        [Display(Name = "Zapamiętaj mnie")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Display(Name = "UserName", ResourceType = typeof(LoginRegisterDisplayNames))]
        [StringLength(8, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.StringLength))]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Display(Name = nameof(LoginRegisterDisplayNames.FirstName), ResourceType = typeof(LoginRegisterDisplayNames))]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Display(Name = nameof(LoginRegisterDisplayNames.LastName), ResourceType = typeof(LoginRegisterDisplayNames))]
        public string LastName { get; set; }

        [Display(Name = nameof(LoginRegisterDisplayNames.Voivodeship), ResourceType = typeof(LoginRegisterDisplayNames))]
        [Range(1, 16, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(LoginRegisterDisplayNames.Voivodeship))]
        public int Voivodeship { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Display(Name = nameof(LoginRegisterDisplayNames.Street), ResourceType = typeof(LoginRegisterDisplayNames))]
        public string Street { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Display(Name = nameof(LoginRegisterDisplayNames.HouseNumber), ResourceType = typeof(LoginRegisterDisplayNames))]
        public string HouseNumber { get; set; }

        [Display(Name = nameof(LoginRegisterDisplayNames.FlatNumber), ResourceType = typeof(LoginRegisterDisplayNames))]
        public string FlatNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Display(Name = nameof(LoginRegisterDisplayNames.Location), ResourceType = typeof(LoginRegisterDisplayNames))]
        public string Location { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Display(Name = nameof(LoginRegisterDisplayNames.PhoneNumber), ResourceType = typeof(LoginRegisterDisplayNames))]
        [Phone(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.PhoneNumber))]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [EmailAddress(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.EmailAddress))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [StringLength(100, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.StringLengthMinMax), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = nameof(LoginRegisterDisplayNames.Password), ResourceType = typeof(LoginRegisterDisplayNames))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = nameof(LoginRegisterDisplayNames.ConfirmPassword), ResourceType = typeof(LoginRegisterDisplayNames))]
        [Compare("Password", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.ConfirmPassword))]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
