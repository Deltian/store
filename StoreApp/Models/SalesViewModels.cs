﻿using StoreApp.Resources;
using StoreApp.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoreApp.Models
{
    public class SearchViewModel
    {
        public string Query { get; set; }

        public int Category { get; set; }
    }

    public class DisplayProductViewModel
    {
        public DisplayProductViewModel()
        {
            Categories = new List<DisplayCategoryViewModel>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public float Price { get; set; }

        public int Amount { get; set; }

        public ICollection<DisplayCategoryViewModel> Categories { get; set; }

        public DisplayPictureViewModel ProfilePicture { get; set; }

        public IEnumerable<DisplayTagViewModel> Tags { get; set; }
    }

    public class DisplayPictureViewModel
    {
        public Guid Id { get; set; }

        public int Order { get; set; }

        public string Extension { get; set; }

    }

    public class DisplayTagViewModel
    {
        public string Name { get; set; }
    }

    public class DisplayCategoryViewModel
    {
        public string Name { get; set; }
    }

    public class AddProductViewModel
    {
        public AddProductViewModel()
        {
            Pictures = new List<PictureViewModel>();
            Tags = new List<string>();
            Categories = new List<CategoryIdViewModel> { new CategoryIdViewModel() };
        }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [StringLength(30, MinimumLength = 10,ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.StringLengthMinMax))]
        [Display(ResourceType = typeof(ProductDisplayNames), Name = nameof(ProductDisplayNames.Name))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [StringLength(250, MinimumLength = 10, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.StringLengthMinMax))]
        [Display(ResourceType = typeof(ProductDisplayNames), Name = nameof(ProductDisplayNames.Description))]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Range(1, double.MaxValue, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.MinRange))]
        [Display(ResourceType = typeof(ProductDisplayNames),Name = nameof(ProductDisplayNames.Price))]
        public float Price { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Required))]
        [Range(1, double.MaxValue, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.MinRange))]
        [Display(ResourceType = typeof(ProductDisplayNames), Name = nameof(ProductDisplayNames.Amount))]
        public int Amount { get; set; }

        
        [Display(ResourceType = typeof(ProductDisplayNames), Name = nameof(ProductDisplayNames.Categories))]
        public List<CategoryIdViewModel> Categories { get; set; }

        [HasValidCategories(ErrorMessage = "Wybrano niepoprawne kategorie")]
        public bool ValidCategories { get; set; }

        [MinimumNumberOfElements(MinimumCount = 3, ErrorMessageResourceName = "Tags", ErrorMessageResourceType =typeof(ErrorMessages))]
        public List<string> Tags { get; set; }

        [MinimumNumberOfElements(MinimumCount = 3, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = nameof(ErrorMessages.Pictures))]
        public List<PictureViewModel> Pictures { get; set; }
    }

    public class CategoryViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<CategoryViewModel> LowerCategories { get; set; }
    }

    public class CategoryIdViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName =nameof(ErrorMessages.SelectListItem))]
        public int Id { get; set; }
    }

    public class PictureViewModel
    {
        public Guid Id { get; set; }

        public string Extension { get; set; }

    }
}