﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StoreApp.Models
{
    public class Category
    {
        public Category()
        {
            LowerCategories = new List<Category>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public Category UpperCategory { get; set; }

        public int? UpperCategoryId { get; set; }

        public ICollection<Category> LowerCategories { get; set; }

        public ICollection<Product> Products { get; set; }


    }

    public class Product
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public float Price { get; set; }

        public int Amount { get; set; }

        public Category Category { get; set; }

        public int CategoryId { get; set; }

        public ICollection<Picture> Pictures { get; set; }

        public ICollection<Tag> Tags { get; set; }
    }

    public class Picture
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public int Order { get; set; }

        public string Extension { get; set; }

        public Guid ProductId { get; set; }

        public Product Product { get; set; }
    }

    public class Tag
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid ProductId { get; set; }

        public Product Product { get; set; }
    }
}